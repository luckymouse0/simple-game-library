#include "Games.h"
#include "Utils.h"

Games::Games()
{
}

Games::~Games()
{
    for (int i = 0; i < GetNumberRows(); i++)
    {
        delete games[i];
    }
    games.clear();
}

int Games::GetNumberRows() const
{
    return games.size();
}

wxString Games::GetValue(int row, int col)
{
    switch (col)
    {
        case 0: return games[row]->GetName();
        case 1: return games[row]->GetSystem();
        case 2: return games[row]->GetGenre();
        case 3: return games[row]->GetCompletionStatus();
        case 4: return games[row]->GetYear(true);
        case 5: return games[row]->GetYear(false);
        case 6: return games[row]->GetRegion();
        case 7: return games[row]->GetPatch();
        default: return wxString();
    }
}

void Games::SetValue(int row, int col, const wxString& value)
{
    switch (col)
    {
        case 0: games[row]->SetName(value); break;
        case 1: games[row]->SetSystem(value); break;
        case 2: games[row]->SetGenre(value); break;
        case 3: games[row]->SetCompletionStatus(value); break;
        case 4: games[row]->SetFinishYear(Utils::GetIntFromString(value)); break;
        case 5: games[row]->SetStartYear(Utils::GetIntFromString(value)); break;
        case 6: games[row]->SetRegion(value); break;
        case 7: games[row]->SetPatch(value); break;
    }
}

Game* Games::GetGame(int row)
{
    return games[row];
}

void Games::AddGame(Game* game)
{
    games.push_back(game);
}

void Games::ModifyGame(int row, Game* game)
{
    delete games[row];
    games[row] = game;
}

void Games::DeleteGame(int row)
{
    delete games[row];
    games.erase(games.begin() + row);
}

void Games::DeleteGames(wxArrayString names)
{
    for (size_t i = 0; i < names.Count(); i++)
    {
        DeleteGame(GetGameRow(names[i]));
    }
}

int Games::GetGameRow(const wxString& name, int ignoreRow)
{
    int row = -1;
    for (int i = 0; i < GetNumberRows(); i++)
    {
        if ((ignoreRow == -1 || ignoreRow != i) && GetValue(i, 0).Cmp(name) == 0)
        {
            row = i;
            break;
        }
    }

    return row;
}
