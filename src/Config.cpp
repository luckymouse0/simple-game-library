#include "Config.h"

Config::Config(wxString configPath)
{
    config = new wxFileConfig("App", wxEmptyString, configPath, wxEmptyString, wxCONFIG_USE_LOCAL_FILE);
}

Config::~Config()
{
    Save();
    delete config;
}

void Config::Save()
{
    config->Flush();
}

wxString Config::ReadKey(wxString key)
{
    wxString value;
    config->Read(key, &value, "");

    return value;
}

void Config::WriteKey(wxString key, wxString value)
{
    config->Write(key, value);
}

void Config::DeleteKey(wxString key)
{
    config->DeleteEntry(key);
}
