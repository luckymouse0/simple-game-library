#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#include "App.h"

//(*AppHeaders
#include "FrameMain.h"
//*)

IMPLEMENT_APP(App);

bool App::OnInit()
{
    //(*AppInitialize
    bool wxsOK = true;
    wxInitAllImageHandlers();
    if ( wxsOK )
    {
    	FrameMain* Frame = new FrameMain(0);
    	Frame->Show();
    	SetTopWindow(Frame);
    }
    //*)

    return wxsOK;
}
