#include "Game.h"

Game::Game(wxString name, wxString system, wxString genre, wxString completionStatus, unsigned int finishYear, unsigned int startYear, wxString region, wxString patch)
{
    SetName(name);
    SetSystem(system);
    SetGenre(genre);
    SetCompletionStatus(completionStatus);
    SetFinishYear(finishYear);
    SetStartYear(startYear);
    SetRegion(region);
    SetPatch(patch);
}

Game::~Game()
{
}

wxString Game::GetName()
{
    return name;
}

void Game::SetName(wxString name)
{
    this->name = name;
}

wxString Game::GetSystem()
{
    return system;
}

void Game::SetSystem(wxString system)
{
    this->system = system;
}

wxString Game::GetGenre()
{
    return genre;
}

void Game::SetGenre(wxString genre)
{
    this->genre = genre;
}

wxString Game::GetCompletionStatus()
{
    return completionStatus;
}

void Game::SetCompletionStatus(wxString completionStatus)
{
    this->completionStatus = completionStatus;
}

unsigned int Game::GetFinishYear()
{
    return finishYear;
}

void Game::SetFinishYear(unsigned int finishYear)
{
    this->finishYear = finishYear;
}

unsigned int Game::GetStartYear()
{
    return startYear;
}

void Game::SetStartYear(unsigned int startYear)
{
    this->startYear = startYear;
}

wxString Game::GetYear(bool isFinishYear)
{
    unsigned int year;

    year = isFinishYear ? GetFinishYear() : GetStartYear();

    return (year > 0) ? wxString(std::to_string(year)) : wxString();
}

wxString Game::GetRegion()
{
    return region;
}

void Game::SetRegion(wxString region)
{
    this->region = region;
}

wxString Game::GetPatch()
{
    return patch;
}

void Game::SetPatch(wxString patch)
{
    this->patch = patch;
}
