#include "GridSorter.h"

#include <algorithm>

GridSorter::GridSorter()
{
    sortDirection.clear();
}

GridSorter::~GridSorter()
{
}

void GridSorter::ToggleSortDirection(int col)
{
    if (sortDirection.find(col) == sortDirection.end())
    {
        sortDirection[col] = true;  // true = ascending, default sorting direction
    }
    else
    {
        sortDirection[col] = !sortDirection[col];   // reverse the sorting direction
    }
}

void GridSorter::SetSortDirection(int col, bool direction)
{
    sortDirection[col] = direction;
}

bool GridSorter::GetSortDirection(int col)
{
    return sortDirection.find(col) == sortDirection.end() ? true : sortDirection[col];
}

void GridSorter::SortData(int col, std::vector<std::vector<std::string>>& rowData, const std::vector<std::string>& pinnedRows)
{
    std::vector<std::vector<std::string>> pinnedRowsData;
    std::vector<std::vector<std::string>> remainingRowsData;

    // Separate the pinned rows and remaining rows
    for (size_t i = 0; i < rowData.size(); ++i)
    {
        const std::vector<std::string>& row = rowData[i];
        if (std::find(pinnedRows.begin(), pinnedRows.end(), row[0]) != pinnedRows.end())
        {
            pinnedRowsData.push_back(row);
        }
        else
        {
            remainingRowsData.push_back(row);
        }
    }

    // Sort the remaining rows
    std::sort(remainingRowsData.begin(), remainingRowsData.end(), [col, this](const std::vector<std::string>& a, const std::vector<std::string>& b)
    {
        return GetSortDirection(col) ? a[col] < b[col] : a[col] > b[col];
    });

    // Concatenate the pinned rows and remaining rows
    rowData = pinnedRowsData;
    rowData.insert(rowData.end(), remainingRowsData.begin(), remainingRowsData.end());
}
