#include "FileHandler.h"
#include "Game.h"
#include "Utils.h"

#include <fstream>
#include <wx/wfstream.h>
#include <wx/txtstrm.h>

namespace FileHandler
{
    bool SaveGames(wxString filename, Games* games, const wxArrayString& colLabels, bool header)
    {
        bool correct = true;

        std::ofstream file(filename.ToStdString());

        if (file.is_open())
        {
            wxString rowString = "";

            if (header)
            {
                for (size_t l = 0; l < colLabels.Count(); l++)
                {
                    rowString.Append(colLabels[l] + ',');
                }
                rowString.RemoveLast();
                file << rowString.ToStdString() << '\n';
            }

            for (int r = 0; r < games->GetNumberRows(); r++)
            {
                rowString = "";
                for (size_t c = 0; c < colLabels.Count(); c++)
                {
                    wxString str(games->GetValue(r, c));
                    str.Replace(",", "|");
                    rowString.Append(str);
                    rowString.Append(',');
                }
                rowString.RemoveLast();
                file << rowString.ToStdString(wxConvUTF8) << '\n';
            }

            file.close();
        }
        else
        {
            correct = false;
        }

        return correct;
    }

    Games* LoadGames(wxString filename, char delimiter, bool header)
    {
        Games* games = new Games();
        wxFileInputStream input(filename);
        wxTextInputStream text(input, wxEmptyString, wxConvUTF8);

        if (input.IsOk())
        {
            if (header)
            {
                wxString dummy = text.ReadLine();
            }

            while(!input.Eof())
            {
                wxString str = text.ReadLine();

                if (!str.IsEmpty())
                {
                    wxArrayString gameArray = SplitString(str, delimiter);

                    for (size_t e = 0; e < gameArray.Count(); e++)
                    {
                        gameArray[e].Replace("|", ",");
                    }

                    games->AddGame(new Game(gameArray[0], gameArray[1], gameArray[2], gameArray[3], Utils::GetIntFromString(gameArray[4]), Utils::GetIntFromString(gameArray[5]), gameArray[6], gameArray[7]));
                }
            }
        }

        return games;
    }

    bool SaveFile(const wxString& filename, const wxArrayString& data)
    {
        bool correct = true;

        std::ofstream file(filename.ToStdString());

        if (file.is_open())
        {
            for (size_t i = 0; i < data.GetCount(); i++)
            {
                file << Utils::ConvertToUTF8(data[i]) << '\n';
            }

            file.close();
        }
        else
        {
            correct = false;
        }

        return correct;
    }

    wxArrayString LoadFile(const wxString& filename)
    {
        wxArrayString strings;

        wxFileInputStream input(filename);
        wxTextInputStream text(input, wxEmptyString, wxConvUTF8);

        while(input.IsOk() && !input.Eof() )
        {
            wxString str = text.ReadLine();

            if (!str.IsEmpty())
            {
                strings.push_back(str);
            }
        }

        return strings;
    }

    wxArrayString SplitString(wxString str, char delimiter)
    {
        wxArrayString arrayString;
        wxString s = "";

        for (const wchar_t& c : str)
        {
            if (c == delimiter)
            {
                arrayString.push_back(wxString(s));
                s = "";
            }
            else
            {
                s += c;
            }
        }

        arrayString.push_back(wxString(s));

        return arrayString;
    }
}
