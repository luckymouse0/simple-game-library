#include "FrameMain.h"
#include "FrameGame.h"
#include "FileHandler.h"
#include "Utils.h"

#include <vector>
#include <string>
#include <wx/icon.h>
#include <wx/bitmap.h>
#include <wx/stdpaths.h>
#include <wx/arrstr.h>
#include <wx/msgdlg.h>
#include <algorithm>

///////////////////////////////////////////////////////////////////////////

FrameMain::FrameMain(wxWindow* parent) : FrameMainView(parent)
{
    SetName("FrameMain");

    gridGames->SetSelectionMode(wxGrid::wxGridSelectRows);

    exePath = Utils::GetExePath();

    SetIcon(wxIcon(exePath + "/res/icons/icon.gif", wxBITMAP_TYPE_GIF));

    config = new Config(exePath + "/config.cfg");

    Utils::LoadFrameConfig(config, this);

    LoadGridConfig();

    gamesPath = exePath + "/games.csv";

    games = FileHandler::LoadGames(gamesPath);

    pinnedPath = exePath + "/favs.csv";
    pinnedGames = FileHandler::LoadFile(pinnedPath);

    UpdateGrid(games);
}

FrameMain::~FrameMain()
{
    delete config;
    delete games;
}

void FrameMain::OnClose(wxCloseEvent& event)
{
    FileHandler::SaveGames(gamesPath, games, Utils::GetColLabels(gridGames));

    if (!this->IsMaximized())
    {
        Utils::SaveFrameConfig(config, this);
    }

    SaveGridConfig();

    FileHandler::SaveFile(pinnedPath, pinnedGames);

    event.Skip();
}

void FrameMain::OnQuitWithoutSave(wxCommandEvent& event)
{
    this->Destroy();
}

Config* FrameMain::GetConfig()
{
    return config;
}

void FrameMain::LoadGridConfig()
{
    wxString keysPrefix = Utils::GetConfigKeyPrefix(this) + "GRID_";

    wxString value;

    sortingColumn = (value = config->ReadKey(keysPrefix + "SORTING_COLUMN")) != "" ? Utils::GetIntFromString(value) : 1;
    // Check for 0 instead, because of ToggleSortDirection in GridSort
    sorter.SetSortDirection(sortingColumn, (value = config->ReadKey(keysPrefix + "SORTING_ASCENDING")) != "" &&  value == "0" ? true : false);
    GridSort(sortingColumn);
}

void FrameMain::SaveGridConfig()
{
    wxString keysPrefix = Utils::GetConfigKeyPrefix(this) + "GRID_";

    if  (sortingColumn >= 0)
    {
        config->WriteKey(keysPrefix + "SORTING_COLUMN", wxString(std::to_string(sortingColumn)));
        config->WriteKey(keysPrefix + "SORTING_ASCENDING", wxString(std::to_string(sorter.GetSortDirection(sortingColumn))));
    }
    else
    {
        config->DeleteKey(keysPrefix + "SORTING_COLUMN");
    }
}

void FrameMain::AddGamesToGrid(Games* games)
{
    // Refresh the grid
    int rows = gridGames->GetNumberRows();
    if (rows > 0)
    {
        gridGames->DeleteRows(0, rows);
    }

    // Add the games to the grid
    for (int row = 0; row < games->GetNumberRows(); row++)
    {
        gridGames->AppendRows(1);
        for (int col = 0; col < gridGames->GetNumberCols(); col++)
        {
            gridGames->SetCellValue(row, col, games->GetValue(row, col));
        }
    }
}

void FrameMain::OnGridColSort(wxGridEvent& event)
{
    GridSort(event.GetCol());
}

void FrameMain::GridSort(int col)
{
    sortingColumn = col;
    if (col >= 0)   // exclude row: column number
    {
        SortByColumn(col);
        gridGames->Refresh();
    }
}

void FrameMain::SortByColumn(int col)
{
    int numRows = gridGames->GetNumberRows();

    std::vector<std::vector<std::string>> rowData(numRows);

    // Get content
    for(int row = 0; row < numRows; row++)
    {
        std::vector<std::string> rowValues(gridGames->GetNumberCols());
        for(int c = 0; c < gridGames->GetNumberCols(); c++)
        {
            rowValues[c] = Utils::ConvertToUTF8(gridGames->GetCellValue(row, c));
        }
        rowData[row] = rowValues;
    }

    sorter.ToggleSortDirection(col);

    std::vector<std::string> vectorGames;
    for (size_t i = 0; i < pinnedGames.GetCount(); i++)
    {
        vectorGames.push_back(Utils::ConvertToUTF8(pinnedGames[i]));
    }

    sorter.SortData(col, rowData, vectorGames);

    // Update grid with sorted row data
    for(int row = 0; row < numRows; row++)
    {
        for(int c = 0; c < gridGames->GetNumberCols(); c++)
        {
            gridGames->SetCellValue(row, c, wxString(rowData[row][c].c_str(), wxConvUTF8));
        }
    }
}

void FrameMain::OnHamburgerClick(wxMouseEvent& event)
{
    wxPoint pos = bpButtonHamburger->GetPosition();
    pos.x += bpButtonHamburger->GetSize().GetX();
    panelMain->PopupMenu(menuHamburger, pos);
}

void FrameMain::OnAddNewGame(wxCommandEvent& event)
{
    this->Disable();
    FrameGame* frameGame = new FrameGame(this, wxT("Add new game"), genres, systems, completionStatus, regions, patches, games);
    frameGame->Show();

    ClearFilters();
}

void FrameMain::OnGamesCellLeftDClick(wxGridEvent& event)
{
    int row = games->GetGameRow(gridGames->GetCellValue(event.GetRow(), 0));

    this->Disable();
    FrameGame* frameGame = new FrameGame(this, wxT("Edit game: " + games->GetGame(row)->GetName()), genres, systems, completionStatus, regions, patches, games, row);
    frameGame->Show();

    ClearFilters();
}

void FrameMain::OnGamesCellRightClick(wxGridEvent& event)
{
    wxArrayString rowValues = Utils::GetSelectedRowValues(gridGames, 0);

    if (rowValues.Count() == 0)
    {
        Utils::EnableRenameMenuItem(menuGridContext, Utils::MenuItemStartWith(menuGridContext, "Pin ") ? "Pin " : "Unpin ", false, "Pin game");
        Utils::EnableRenameMenuItem(menuGridContext, "Delete ", false, "Delete game");
    }
    else if (rowValues.Count() >= 1)
    {
        if (rowValues.Count() == 1)
        {
            wxString pinString = "Unpin ";
            if (Utils::IsNewElement(&pinnedGames, rowValues[0]))
            {
                pinString = "Pin ";
            }
            Utils::EnableRenameMenuItem(menuGridContext, Utils::MenuItemStartWith(menuGridContext, "Pin ") ? "Pin " : "Unpin ", true, pinString + wxT("game: \"" + rowValues[0] + "\""));

            Utils::EnableRenameMenuItem(menuGridContext, "Delete ", true, wxT("Delete game: \"" + rowValues[0] + "\""));
        }
        else
        {
            Utils::EnableRenameMenuItem(menuGridContext, Utils::MenuItemStartWith(menuGridContext, "Pin ") ? "Pin " : "Unpin ", true, "Pin / Unpin selected games");
            Utils::EnableRenameMenuItem(menuGridContext, "Delete ", true, "Delete selected games");
        }
    }

    if (pinnedGames.Count() > 0)
    {
        Utils::EnableRenameMenuItem(menuGridContext, "Unpin all", true, "");
    }
    else
    {
        Utils::EnableRenameMenuItem(menuGridContext, "Unpin all", false, "");
    }

    panelMain->PopupMenu(menuGridContext, ScreenToClient(wxGetMousePosition()));
}

void FrameMain::OnSelectAllRows(wxCommandEvent& event)
{
    gridGames->ClearSelection();

    for (int row = 0; row < gridGames->GetNumberRows(); ++row)
    {
        if (gridGames->IsRowShown(row))
        {
            gridGames->SelectRow(row, true);
        }
    }
}

void FrameMain::OnPinRows(wxCommandEvent& event)
{
    wxArrayString rowValues = Utils::GetSelectedRowValues(gridGames, 0);

    if (rowValues.Count() >= 1)
    {
        for (size_t i = 0; i < rowValues.Count(); i++)
        {
            if (Utils::IsNewElement(&pinnedGames, rowValues[i]))
            {
                Utils::AddNewElement(&pinnedGames, rowValues[i]);
            }
            else
            {
                pinnedGames.Remove(rowValues[i]);
            }
        }

        SetPinnedRows();
    }
}

void FrameMain::OnUnpinAll(wxCommandEvent& event)
{
    pinnedGames.Clear();
    SetPinnedRows();
}

void FrameMain::OnDeleteRows(wxCommandEvent& event)
{
    DeleteRows();
}

void FrameMain::OnGamesKeyDown(wxKeyEvent& event)
{
    if (event.GetKeyCode() == WXK_DELETE && gridGames->IsSelection())
    {
        DeleteRows();
    }
    else
    {
        event.Skip();
    }
}

void FrameMain::DeleteRows()
{
    wxArrayString rowValues = Utils::GetSelectedRowValues(gridGames, 0);

    if (rowValues.Count() >= 1)
    {
        wxMessageDialog dialog(this, rowValues.Count() == 1 ? wxT("Do you want to remove: " + rowValues[0] + "?") : "Do you want to remove selected games?", rowValues.Count() == 1 ? "Confirm: Remove game" :  "Confirm: Remove games", wxYES_NO | wxICON_QUESTION);
        if (dialog.ShowModal() == wxID_YES)
        {
            if (rowValues.Count() == 1)
            {
                games->DeleteGame(games->GetGameRow(rowValues[0]));
            }
            else
            {
                games->DeleteGames(rowValues);
            }

            UpdateGrid(games);
        }
    }
}

void FrameMain::OnSearchFilter(wxCommandEvent& event)
{
    FilterGrid(choiceFilter->GetString(choiceFilter->GetSelection()),listBoxFilter->GetStringSelection(), searchCtrlFilter->GetValue());
}

void FrameMain::OnChoiceFilter(wxCommandEvent& event)
{
    listBoxFilter->Clear();
    wxString choice = event.GetString();

    if (choice == "System")
    {
        listBoxFilter->Set(Utils::GetCategoriesCount(systems, Utils::GetColumnValues(gridGames, Utils::GetColIndex(gridGames, "System"))));
    }
    else if (choice == "Genre")
    {
        listBoxFilter->Set(Utils::GetCategoriesCount(genres, Utils::GetColumnValues(gridGames, Utils::GetColIndex(gridGames, "Genre"))));
    }
    else if (choice == "Completion Status")
    {
        listBoxFilter->Set(Utils::GetCategoriesCount(completionStatus, Utils::GetColumnValues(gridGames, Utils::GetColIndex(gridGames, "Completion Status"))));
    }
    else if (choice == "Finish Year")
    {
        listBoxFilter->Set(Utils::GetCategoriesCount(finishYears, Utils::GetColumnValues(gridGames, Utils::GetColIndex(gridGames, "Finish Year"))));
    }
    else if (choice == "Start Year")
    {
        listBoxFilter->Set(Utils::GetCategoriesCount(startYears, Utils::GetColumnValues(gridGames, Utils::GetColIndex(gridGames, "Start Year"))));
    }
    else if (choice == "Region")
    {
        listBoxFilter->Set(Utils::GetCategoriesCount(regions, Utils::GetColumnValues(gridGames, Utils::GetColIndex(gridGames, "Region"))));
    }
    else if (choice == "Patch")
    {
        listBoxFilter->Set(Utils::GetCategoriesCount(patches, Utils::GetColumnValues(gridGames, Utils::GetColIndex(gridGames, "Patch"))));
    }
    else
    {
        searchCtrlFilter->SetValue("");
    }
}

void FrameMain::OnListBoxFilter(wxCommandEvent& event)
{
    FilterGrid(choiceFilter->GetString(choiceFilter->GetSelection()), Utils::RemoveSubstring(listBoxFilter->GetStringSelection(), "] "), searchCtrlFilter->GetValue());
}

void FrameMain::FilterGrid(const wxString& column, const wxString& columnFilter, const wxString& searchFilter)
{
    gridGames->ClearSelection();
    int index = Utils::GetColIndex(gridGames, column);
    for (int row = 0; row < gridGames->GetNumberRows(); row++)
    {
        if (static_cast<size_t>(row) < pinnedGames.size())
        {
            gridGames->ShowRow(row);
        }
        else if (index == -1 || gridGames->GetCellValue(row, index) == columnFilter)
        {
            if (searchFilter.IsEmpty() || (!searchFilter.IsEmpty() && gridGames->GetCellValue(row, 0).Lower().Contains(searchFilter.Lower())))
            {
                gridGames->ShowRow(row);
            }
            else
            {
                gridGames->HideRow(row);
            }
        }
        else
        {
            gridGames->HideRow(row);
        }
    }
}

void FrameMain::ClearFilters()
{
    choiceFilter->SetSelection(0);
    listBoxFilter->Clear();
    searchCtrlFilter->SetValue("");
    gridGames->ClearSelection();
}

void FrameMain::UpdateGrid(Games* games)
{
    AddGamesToGrid(games);
    UpdateLists(games);
    UpdatePinned(games);

    gridGames->AutoSizeColumns(true);
    gridGames->SetRowLabelSize(wxGRID_AUTOSIZE);
}

void FrameMain::UpdateLists(Games* games)
{
    systems.Clear();
    genres.Clear();
    completionStatus.Clear();
    finishYears.Clear();
    startYears.Clear();
    regions.Clear();
    patches.Clear();

    systems = FileHandler::LoadFile(exePath + "/res/systems.csv");
    genres = FileHandler::LoadFile(exePath + "/res/genres.csv");
    completionStatus = FileHandler::LoadFile(exePath + "/res/completion.csv");
    regions = FileHandler::LoadFile(exePath + "/res/regions.csv");
    patches = FileHandler::LoadFile(exePath + "/res/patches.csv");

    for (int row = 0; row < games->GetNumberRows(); row++)
    {
        Utils::AddNewElement(&systems, games->GetValue(row, 1));
        Utils::AddNewElement(&genres, games->GetValue(row, 2));
        Utils::AddNewElement(&completionStatus, games->GetValue(row, 3));
        Utils::AddNewElement(&finishYears, games->GetValue(row, 4));
        Utils::AddNewElement(&startYears, games->GetValue(row, 5));
        Utils::AddNewElement(&regions, games->GetValue(row, 6));
        Utils::AddNewElement(&patches, games->GetValue(row, 7));
    }

    finishYears.Sort();
    startYears.Sort();
}

void FrameMain::UpdatePinned(Games* games)
{
    for (size_t i = 0; i < pinnedGames.GetCount(); i++)
    {
        if (games->GetGameRow(pinnedGames[i]) == -1)
        {
            pinnedGames.RemoveAt(i);
        }
    }

    SetPinnedRows();
}

void FrameMain::SetPinnedRows()
{
    GridSort(sortingColumn);
    GridSort(sortingColumn); // twice to get the original order

    SetPinnedRowsColor();

    ClearFilters();
}

void FrameMain::SetPinnedRowsColor()
{
    for (int row = 0; row < gridGames->GetNumberRows(); row++)
    {
        for (int col = 0; col < gridGames->GetNumberCols(); col++)
        {
            if (static_cast<size_t>(row) < pinnedGames.size())
            {
                gridGames->SetCellBackgroundColour(row, col, wxColour(128, 128, 128));
            }
            else
            {
                gridGames->SetCellBackgroundColour(row, col, gridGames->GetDefaultCellBackgroundColour());
            }
        }
    }
}
