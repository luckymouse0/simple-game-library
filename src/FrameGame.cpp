#include "FrameGame.h"
#include "Utils.h"
#include "Game.h"

#include <wx/icon.h>
#include <wx/bitmap.h>
#include <wx/datetime.h>
#include <wx/msgdlg.h>

FrameGame::FrameGame(FrameMain* parent, const wxString& title, const wxArrayString& genres, const wxArrayString& systems, const wxArrayString& completionStatus, const wxArrayString& regions, const wxArrayString& patches, Games* games, const int& row) : FrameGameView(dynamic_cast<wxWindow*>(parent))
{
    initialized = false;
    SetName("FrameGame");
    SetTitle(title);
    Utils::LoadFrameConfig(parent->GetConfig(), this);

    this->parent = parent;
    this->games = games;
    this->genres = const_cast<wxArrayString*>(&genres);
    this->systems = const_cast<wxArrayString*>(&systems);
    this->completionStatus = const_cast<wxArrayString*>(&completionStatus);
    this->regions = const_cast<wxArrayString*>(&regions);
    this->patches = const_cast<wxArrayString*>(&patches);
    this->row = row;
    bcomboBoxSystem->Set(systems);
    bcomboBoxGenre->Set(genres);
    bcomboBoxCompletionStatus->Set(completionStatus);
    bcomboBoxRegion->Set(regions);
    bcomboBoxPatch->Set(patches);

    Utils::AddComboBoxIcons(bcomboBoxSystem, Utils::GetExePath() + "/res/systems/", 12, 12, "Unknown.gif");
    Utils::AddComboBoxIcons(bcomboBoxGenre, Utils::GetExePath() + "/res/genres/", 12, 12);
    Utils::AddComboBoxIcons(bcomboBoxCompletionStatus, Utils::GetExePath() + "/res/completion/", 12, 12);
    Utils::AddComboBoxIcons(bcomboBoxRegion, Utils::GetExePath() + "/res/regions/", 12, 12);
    Utils::AddComboBoxIcons(bcomboBoxPatch, Utils::GetExePath() + "/res/patches/", 12, 12);

    if (row != -1)
    {
        SetIcon(wxIcon(Utils::GetExePath() + "/res/icons/icon-edit.gif", wxBITMAP_TYPE_GIF));

        bpButtonDelete->Enable();

        textCtrlName->SetValue(games->GetGame(row)->GetName());
        bcomboBoxSystem->SetStringSelection(games->GetGame(row)->GetSystem());
        bcomboBoxGenre->SetStringSelection(games->GetGame(row)->GetGenre());
        bcomboBoxCompletionStatus->SetStringSelection(games->GetGame(row)->GetCompletionStatus());
        SetSpinCtrlValue(spinCtrlFinishYear, checkBoxFinishYear, games->GetGame(row)->GetFinishYear());
        SetSpinCtrlValue(spinCtrlStartYear, checkBoxStartYear, games->GetGame(row)->GetStartYear());

        if (games->GetGame(row)->GetFinishYear() != 0)
        {
            spinCtrlStartYear->SetRange(spinCtrlStartYear->GetMin(), games->GetGame(row)->GetFinishYear());
        }

        if (games->GetGame(row)->GetRegion() != "")
        {
            checkBoxRegion->SetValue(true);
            bcomboBoxRegion->Enable(true);
            bcomboBoxRegion->SetStringSelection(games->GetGame(row)->GetRegion());
        }

        if (games->GetGame(row)->GetPatch() != "")
        {
            checkBoxPatch->SetValue(true);
            bcomboBoxPatch->Enable(true);
            bcomboBoxPatch->SetStringSelection(games->GetGame(row)->GetPatch());
        }
    }
    else
    {
        SetIcon(wxIcon(Utils::GetExePath() + "/res/icons/icon-add.gif", wxBITMAP_TYPE_GIF));

        spinCtrlFinishYear->SetValue(wxDateTime::Now().GetYear());
        spinCtrlStartYear->SetValue(wxDateTime::Now().GetYear());
    }

    spinCtrlFinishYear->SetRange(spinCtrlFinishYear->GetMin(), wxDateTime::Now().GetYear());
    spinCtrlStartYear->SetRange(spinCtrlStartYear->GetMin(), wxDateTime::Now().GetYear());

    SetMinSize(Utils::GetMinimumSize(this));
    Fit();
    initialized = true;
}

FrameGame::~FrameGame()
{
    parent->Enable();
    parent->Raise();
}

void FrameGame::OnClose(wxCloseEvent& event)
{
    bool close = true;
    if (event.CanVeto() && event.GetEventType() == wxEVT_CLOSE_WINDOW)
    {
        if (IsEdited())
        {
            wxMessageDialog dialog(this, "Do you want to close without saving?", "Confirm: Close without saving", wxYES_NO | wxICON_QUESTION);
            if (dialog.ShowModal() != wxID_YES)
            {
                close = false;
            }
        }
    }

    if (close)
    {
        if (!this->IsMaximized())
        {
            Utils::SaveFrameConfig(parent->GetConfig(), this);
        }

        event.Skip();
    }
    else
    {
        event.Veto();
    }
}

void FrameGame::SetSpinCtrlValue(wxSpinCtrl* spinCtrl, wxCheckBox* checkBoxYear, int year)
{
    if (year > 0)
    {
        checkBoxYear->SetValue(true);
        spinCtrl->SetValue(year);
        spinCtrl->Enable(true);
    }
    else
    {
        spinCtrl->SetValue(wxDateTime::Now().GetYear());
    }
}

void FrameGame::OnFinishYearChecked(wxCommandEvent& event)
{
    spinCtrlFinishYear->Enable(checkBoxFinishYear->GetValue());
    UpdateSpinCtrlValues();
    SetEdited();
}

void FrameGame::OnStartYearChecked(wxCommandEvent& event)
{
    spinCtrlStartYear->Enable(checkBoxStartYear->GetValue());
    UpdateSpinCtrlValues();
    SetEdited();
}

void FrameGame::OnRegionChecked(wxCommandEvent& event)
{
    bcomboBoxRegion->Enable(checkBoxRegion->GetValue());
    SetEdited();

    if (bcomboBoxRegion->GetSelection() == wxNOT_FOUND)
    {
        bcomboBoxRegion->SetSelection(0);
    }
}

void FrameGame::OnPatchChecked(wxCommandEvent& event)
{
    bcomboBoxPatch->Enable(checkBoxPatch->GetValue());
    SetEdited();

    if (bcomboBoxPatch->GetSelection() == wxNOT_FOUND)
    {
        bcomboBoxPatch->SetSelection(0);
    }
}

void FrameGame::OnOKClick(wxCommandEvent& event)
{
    if (CheckValues() && CheckComboElements())
    {
        Game* game = new Game(textCtrlName->GetValue(), bcomboBoxSystem->GetValue(), bcomboBoxGenre->GetValue(), bcomboBoxCompletionStatus->GetValue(), checkBoxFinishYear->GetValue() ? spinCtrlFinishYear->GetValue() : 0, checkBoxStartYear->GetValue() ? spinCtrlStartYear->GetValue() : 0, checkBoxRegion->GetValue() ? bcomboBoxRegion->GetValue() : "", checkBoxPatch->GetValue() ? bcomboBoxPatch->GetValue() : "");

        if (row == -1)
        {
            games->AddGame(game);
        }
        else
        {
            if (IsEdited())
            {
                wxMessageDialog dialog(this, "Do you want to save changes?", "Confirm: Edit game", wxYES_NO | wxICON_QUESTION);
                if (dialog.ShowModal() == wxID_YES)
                {
                    games->ModifyGame(row, game);
                    SetEdited(false);
                }
            }
            else
            {
                Close();
            }
        }

        parent->UpdateGrid(games);

        Close();
    }
}

void FrameGame::OnCancelClick(wxCommandEvent& event)
{
    Close();
}

void FrameGame::OnDeleteClick(wxCommandEvent& event)
{
    wxMessageDialog dialog(this, "Do you want to remove: " + games->GetGame(row)->GetName() + "?", "Confirm: Remove game", wxYES_NO | wxICON_QUESTION);
    if (dialog.ShowModal() == wxID_YES)
    {
        games->DeleteGame(row);
        parent->UpdateGrid(games);
        Close();
    }
}

void FrameGame::OnKeyPressed(wxKeyEvent& event)
{
    SetEdited();
    event.Skip();
}

void FrameGame::OnCombobox(wxCommandEvent& event)
{
    if ((event.GetEventType() == wxEVT_COMMAND_COMBOBOX_SELECTED || event.GetEventType() == wxEVT_COMMAND_TEXT_UPDATED) && event.GetSelection() != wxNOT_FOUND)
    {
        SetEdited();
    }

    event.Skip();
}

void FrameGame::OnSpinCtrl(wxSpinEvent& event)
{
    SetEdited();
    UpdateSpinCtrlValues();
    event.Skip();
}

bool FrameGame::IsEdited()
{
    return GetTitle().starts_with('*');
}

void FrameGame::SetEdited(bool changed)
{
    if (row != -1 && initialized)
    {
        if (changed && !IsEdited())
        {
            SetTitle("*" + GetTitle());
        }
        else if (!changed && IsEdited())
        {
            SetTitle(GetTitle().Remove(0));
        }
    }
}

void FrameGame::UpdateSpinCtrlValues()
{
    if (checkBoxFinishYear->GetValue() && checkBoxStartYear->GetValue() && spinCtrlFinishYear->GetValue() < spinCtrlStartYear->GetValue())
    {
        spinCtrlStartYear->SetValue(spinCtrlFinishYear->GetValue());
    }
}

bool FrameGame::CheckValues()
{
    bool correct = false;
    wxString name = textCtrlName->GetValue();

    if (games->GetGameRow(name) != -1 && row == -1)
    {
        wxMessageBox("Can't add new game, game already exists! Modify existing game instead.", "Game exists", wxOK | wxICON_INFORMATION);
    }
    else if (games->GetGameRow(name, row) != -1 && row != -1)
    {
        wxMessageBox("Can't modify game, name already exists in another entry! Use a different name.", "Name exists", wxOK | wxICON_INFORMATION);
    }
    else if (textCtrlName->GetValue() == "")
    {
        wxMessageBox("Name value is empty, add a name first.", "Empty Name", wxOK | wxICON_EXCLAMATION);
    }
    else if (bcomboBoxSystem->GetValue() == "")
    {
        wxMessageBox("System value is empty, add a system first.", "Empty System", wxOK | wxICON_EXCLAMATION);
    }
    else if (bcomboBoxGenre->GetValue() == "")
    {
        wxMessageBox("Genre value is empty, add a genre first.", "Empty Genre", wxOK | wxICON_EXCLAMATION);
    }
    else if (bcomboBoxCompletionStatus->GetValue() == "")
    {
        wxMessageBox("Completion status value is empty, add a completion status first.", "Empty Completion Status", wxOK | wxICON_EXCLAMATION);
    }
    else if (checkBoxRegion->GetValue() && bcomboBoxRegion->GetValue() == "")
    {
        wxMessageBox("Region value is empty, add a region or uncheck it.", "Empty Region", wxOK | wxICON_EXCLAMATION);
    }
    else if (checkBoxPatch->GetValue() && bcomboBoxPatch->GetValue() == "")
    {
        wxMessageBox("Patch value is empty, add a patch or uncheck it.", "Empty Patch", wxOK | wxICON_EXCLAMATION);
    }
    else
    {
        correct = true;
    }

    return correct;
}

bool FrameGame::CheckComboElements()
{
    bool correct = true;

    if (Utils::IsNewComboElement(bcomboBoxSystem))
    {
        correct = Utils::ConfirmAddNewComboElement(this, bcomboBoxSystem, "system", systems);
    }

    if (correct && Utils::IsNewComboElement(bcomboBoxGenre))
    {
        correct = Utils::ConfirmAddNewComboElement(this, bcomboBoxGenre, "genre", genres);
    }

    if (correct && Utils::IsNewComboElement(bcomboBoxCompletionStatus))
    {
        correct = Utils::ConfirmAddNewComboElement(this, bcomboBoxCompletionStatus, "completion status", completionStatus);
    }

    if (correct && checkBoxRegion->GetValue() && Utils::IsNewComboElement(bcomboBoxRegion))
    {
        correct = Utils::ConfirmAddNewComboElement(this, bcomboBoxRegion, "region", regions);
    }

    if (correct && checkBoxPatch->GetValue() && Utils::IsNewComboElement(bcomboBoxPatch))
    {
        correct = Utils::ConfirmAddNewComboElement(this, bcomboBoxPatch, "patch", patches);
    }

    return correct;
}
