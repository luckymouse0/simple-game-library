///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.10.1)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#include "Views.h"

///////////////////////////////////////////////////////////////////////////

FrameMainView::FrameMainView( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxSize( -1,-1 ), wxDefaultSize );
	this->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOW ) );
	this->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_GRAYTEXT ) );

	wxBoxSizer* bSizerMain;
	bSizerMain = new wxBoxSizer( wxVERTICAL );

	panelMain = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxGridBagSizer* gbSizerMain;
	gbSizerMain = new wxGridBagSizer( 0, 0 );
	gbSizerMain->SetFlexibleDirection( wxBOTH );
	gbSizerMain->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	searchCtrlFilter = new wxSearchCtrl( panelMain, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 264,28 ), 0 );
	#ifndef __WXMAC__
	searchCtrlFilter->ShowSearchButton( true );
	#endif
	searchCtrlFilter->ShowCancelButton( true );
	searchCtrlFilter->SetFont( wxFont( 10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxEmptyString ) );

	gbSizerMain->Add( searchCtrlFilter, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	wxString choiceFilterChoices[] = { wxT("No Filter"), wxT("System"), wxT("Genre"), wxT("Completion Status"), wxT("Finish Year"), wxT("Start Year"), wxT("Region"), wxT("Patch") };
	int choiceFilterNChoices = sizeof( choiceFilterChoices ) / sizeof( wxString );
	choiceFilter = new wxChoice( panelMain, wxID_ANY, wxDefaultPosition, wxSize( 298,31 ), choiceFilterNChoices, choiceFilterChoices, 0 );
	choiceFilter->SetSelection( 0 );
	choiceFilter->SetFont( wxFont( 10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxEmptyString ) );
	choiceFilter->SetToolTip( wxT("Select a filter category.") );

	gbSizerMain->Add( choiceFilter, wxGBPosition( 1, 0 ), wxGBSpan( 1, 2 ), wxLEFT|wxTOP, 5 );

	bpButtonHamburger = new wxBitmapButton( panelMain, wxID_ANY, wxNullBitmap, wxPoint( -1,-1 ), wxSize( 28,28 ), wxBU_AUTODRAW|0 );

	bpButtonHamburger->SetBitmap( wxBitmap( wxT("res/buttons/bars.png"), wxBITMAP_TYPE_ANY ) );
	gbSizerMain->Add( bpButtonHamburger, wxGBPosition( 0, 1 ), wxGBSpan( 1, 1 ), wxALIGN_CENTER_VERTICAL|wxLEFT, 2 );

	listBoxFilter = new wxListBox( panelMain, wxID_ANY, wxDefaultPosition, wxSize( 298,361 ), 0, NULL, 0|wxBORDER_SUNKEN );
	gbSizerMain->Add( listBoxFilter, wxGBPosition( 2, 0 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND, 5 );

	gridGames = new wxGrid( panelMain, wxID_ANY, wxDefaultPosition, wxSize( 603,425 ), wxBORDER_SUNKEN|wxHSCROLL|wxVSCROLL );

	// Grid
	gridGames->CreateGrid( 0, 8 );
	gridGames->EnableEditing( false );
	gridGames->EnableGridLines( true );
	gridGames->SetGridLineColour( wxSystemSettings::GetColour( wxSYS_COLOUR_SCROLLBAR ) );
	gridGames->EnableDragGridSize( false );
	gridGames->SetMargins( 0, 0 );

	// Columns
	gridGames->SetColSize( 0, 80 );
	gridGames->SetColSize( 1, 80 );
	gridGames->SetColSize( 2, 80 );
	gridGames->SetColSize( 3, 80 );
	gridGames->SetColSize( 4, 125 );
	gridGames->AutoSizeColumns();
	gridGames->EnableDragColMove( true );
	gridGames->EnableDragColSize( true );
	gridGames->SetColLabelValue( 0, wxT("Name") );
	gridGames->SetColLabelValue( 1, wxT("System") );
	gridGames->SetColLabelValue( 2, wxT("Genre") );
	gridGames->SetColLabelValue( 3, wxT("Completion Status") );
	gridGames->SetColLabelValue( 4, wxT("Finish Year") );
	gridGames->SetColLabelValue( 5, wxT("Start Year") );
	gridGames->SetColLabelValue( 6, wxT("Region") );
	gridGames->SetColLabelValue( 7, wxT("Patch") );
	gridGames->SetColLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Rows
	gridGames->AutoSizeRows();
	gridGames->EnableDragRowSize( true );
	gridGames->SetRowLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Label Appearance

	// Cell Defaults
	gridGames->SetDefaultCellAlignment( wxALIGN_CENTER, wxALIGN_CENTER );
	gbSizerMain->Add( gridGames, wxGBPosition( 0, 4 ), wxGBSpan( 3, 1 ), wxALL|wxEXPAND, 5 );


	gbSizerMain->AddGrowableCol( 4 );
	gbSizerMain->AddGrowableRow( 2 );

	panelMain->SetSizer( gbSizerMain );
	panelMain->Layout();
	gbSizerMain->Fit( panelMain );
	menuGridContext = new wxMenu();
	wxMenuItem* menuItemSelectAllRows;
	menuItemSelectAllRows = new wxMenuItem( menuGridContext, wxID_ANY, wxString( wxT("Select all") ) , wxEmptyString, wxITEM_NORMAL );
	menuGridContext->Append( menuItemSelectAllRows );

	menuGridContext->AppendSeparator();

	wxMenuItem* menuItemPinRows;
	menuItemPinRows = new wxMenuItem( menuGridContext, wxID_ANY, wxString( wxT("Pin game") ) , wxEmptyString, wxITEM_NORMAL );
	menuGridContext->Append( menuItemPinRows );

	wxMenuItem* menuItemUnpinAll;
	menuItemUnpinAll = new wxMenuItem( menuGridContext, wxID_ANY, wxString( wxT("Unpin all") ) , wxEmptyString, wxITEM_NORMAL );
	menuGridContext->Append( menuItemUnpinAll );

	menuGridContext->AppendSeparator();

	wxMenuItem* menuItemDeleteRows;
	menuItemDeleteRows = new wxMenuItem( menuGridContext, wxID_ANY, wxString( wxT("Delete game") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	menuItemDeleteRows->SetBitmaps( wxNullBitmap );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	menuItemDeleteRows->SetBitmap( wxNullBitmap );
	#endif
	menuGridContext->Append( menuItemDeleteRows );

	panelMain->Connect( wxEVT_RIGHT_DOWN, wxMouseEventHandler( FrameMainView::panelMainOnContextMenu ), NULL, this );

	bSizerMain->Add( panelMain, 1, wxALL|wxEXPAND, 5 );


	this->SetSizer( bSizerMain );
	this->Layout();
	menuHamburger = new wxMenu();
	wxMenuItem* menuItemAddGame;
	menuItemAddGame = new wxMenuItem( menuHamburger, wxID_ANY, wxString( wxT("Add new game") ) , wxEmptyString, wxITEM_NORMAL );
	menuHamburger->Append( menuItemAddGame );

	menuHamburger->AppendSeparator();

	wxMenuItem* menuItemQuitWithoutSave;
	menuItemQuitWithoutSave = new wxMenuItem( menuHamburger, wxID_ANY, wxString( wxT("Quit without saving") ) , wxEmptyString, wxITEM_NORMAL );
	menuHamburger->Append( menuItemQuitWithoutSave );



	this->Centre( wxBOTH );

	// Connect Events
	this->Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( FrameMainView::OnClose ) );
	searchCtrlFilter->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( FrameMainView::OnSearchFilter ), NULL, this );
	choiceFilter->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( FrameMainView::OnChoiceFilter ), NULL, this );
	bpButtonHamburger->Connect( wxEVT_LEFT_DOWN, wxMouseEventHandler( FrameMainView::OnHamburgerClick ), NULL, this );
	listBoxFilter->Connect( wxEVT_COMMAND_LISTBOX_SELECTED, wxCommandEventHandler( FrameMainView::OnListBoxFilter ), NULL, this );
	gridGames->Connect( wxEVT_GRID_CELL_LEFT_DCLICK, wxGridEventHandler( FrameMainView::OnGamesCellLeftDClick ), NULL, this );
	gridGames->Connect( wxEVT_GRID_CELL_RIGHT_CLICK, wxGridEventHandler( FrameMainView::OnGamesCellRightClick ), NULL, this );
	gridGames->Connect( wxEVT_GRID_LABEL_LEFT_CLICK, wxGridEventHandler( FrameMainView::OnGridColSort ), NULL, this );
	gridGames->Connect( wxEVT_KEY_DOWN, wxKeyEventHandler( FrameMainView::OnGamesKeyDown ), NULL, this );
	menuGridContext->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( FrameMainView::OnSelectAllRows ), this, menuItemSelectAllRows->GetId());
	menuGridContext->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( FrameMainView::OnPinRows ), this, menuItemPinRows->GetId());
	menuGridContext->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( FrameMainView::OnUnpinAll ), this, menuItemUnpinAll->GetId());
	menuGridContext->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( FrameMainView::OnDeleteRows ), this, menuItemDeleteRows->GetId());
	menuHamburger->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( FrameMainView::OnAddNewGame ), this, menuItemAddGame->GetId());
	menuHamburger->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( FrameMainView::OnQuitWithoutSave ), this, menuItemQuitWithoutSave->GetId());
}

FrameMainView::~FrameMainView()
{
	// Disconnect Events
	this->Disconnect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( FrameMainView::OnClose ) );
	searchCtrlFilter->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( FrameMainView::OnSearchFilter ), NULL, this );
	choiceFilter->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( FrameMainView::OnChoiceFilter ), NULL, this );
	bpButtonHamburger->Disconnect( wxEVT_LEFT_DOWN, wxMouseEventHandler( FrameMainView::OnHamburgerClick ), NULL, this );
	listBoxFilter->Disconnect( wxEVT_COMMAND_LISTBOX_SELECTED, wxCommandEventHandler( FrameMainView::OnListBoxFilter ), NULL, this );
	gridGames->Disconnect( wxEVT_GRID_CELL_LEFT_DCLICK, wxGridEventHandler( FrameMainView::OnGamesCellLeftDClick ), NULL, this );
	gridGames->Disconnect( wxEVT_GRID_CELL_RIGHT_CLICK, wxGridEventHandler( FrameMainView::OnGamesCellRightClick ), NULL, this );
	gridGames->Disconnect( wxEVT_GRID_LABEL_LEFT_CLICK, wxGridEventHandler( FrameMainView::OnGridColSort ), NULL, this );
	gridGames->Disconnect( wxEVT_KEY_DOWN, wxKeyEventHandler( FrameMainView::OnGamesKeyDown ), NULL, this );

	delete menuGridContext;
	delete menuHamburger;
}

FrameGameView::FrameGameView( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxSize( -1,-1 ), wxDefaultSize );

	wxBoxSizer* bSizerGame;
	bSizerGame = new wxBoxSizer( wxVERTICAL );

	panelGame = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxGridBagSizer* gbSizerGame;
	gbSizerGame = new wxGridBagSizer( 5, 0 );
	gbSizerGame->SetFlexibleDirection( wxBOTH );
	gbSizerGame->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	staticTextName = new wxStaticText( panelGame, wxID_ANY, wxT("Name"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextName->Wrap( -1 );
	gbSizerGame->Add( staticTextName, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxALL, 5 );

	textCtrlName = new wxTextCtrl( panelGame, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 395,-1 ), 0 );
	textCtrlName->SetFont( wxFont( 10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxEmptyString ) );

	gbSizerGame->Add( textCtrlName, wxGBPosition( 0, 1 ), wxGBSpan( 1, 6 ), wxRIGHT, 20 );

	staticTextSystem = new wxStaticText( panelGame, wxID_ANY, wxT("System"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextSystem->Wrap( -1 );
	gbSizerGame->Add( staticTextSystem, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxALL, 5 );

	bcomboBoxSystem = new wxBitmapComboBox( panelGame, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 260,-1 ), 0, NULL, 0 );
	bcomboBoxSystem->SetFont( wxFont( 10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxEmptyString ) );

	gbSizerGame->Add( bcomboBoxSystem, wxGBPosition( 1, 1 ), wxGBSpan( 1, 4 ), 0, 5 );

	staticTextGenre = new wxStaticText( panelGame, wxID_ANY, wxT("Genre"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextGenre->Wrap( -1 );
	gbSizerGame->Add( staticTextGenre, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxALL, 5 );

	bcomboBoxGenre = new wxBitmapComboBox( panelGame, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 260,-1 ), 0, NULL, 0 );
	bcomboBoxGenre->SetFont( wxFont( 10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxEmptyString ) );

	gbSizerGame->Add( bcomboBoxGenre, wxGBPosition( 2, 1 ), wxGBSpan( 1, 4 ), 0, 5 );

	staticTextCompletionStatus = new wxStaticText( panelGame, wxID_ANY, wxT("Completion Status"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextCompletionStatus->Wrap( -1 );
	gbSizerGame->Add( staticTextCompletionStatus, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	bcomboBoxCompletionStatus = new wxBitmapComboBox( panelGame, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 260,-1 ), 0, NULL, 0 );
	bcomboBoxCompletionStatus->SetSelection( 0 );
	bcomboBoxCompletionStatus->SetFont( wxFont( 10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxEmptyString ) );

	gbSizerGame->Add( bcomboBoxCompletionStatus, wxGBPosition( 3, 1 ), wxGBSpan( 1, 4 ), 0, 5 );

	staticTextFinishYear = new wxStaticText( panelGame, wxID_ANY, wxT("FInish Year"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextFinishYear->Wrap( -1 );
	gbSizerGame->Add( staticTextFinishYear, wxGBPosition( 4, 0 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxALL, 5 );

	checkBoxFinishYear = new wxCheckBox( panelGame, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	checkBoxFinishYear->SetToolTip( wxT("Has finish year") );

	gbSizerGame->Add( checkBoxFinishYear, wxGBPosition( 4, 1 ), wxGBSpan( 1, 1 ), wxBOTTOM|wxRIGHT|wxTOP, 5 );

	spinCtrlFinishYear = new wxSpinCtrl( panelGame, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( -1,-1 ), wxALIGN_CENTER_HORIZONTAL|wxSP_ARROW_KEYS, 1900, 9999, 1900 );
	spinCtrlFinishYear->SetFont( wxFont( 10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxEmptyString ) );
	spinCtrlFinishYear->Enable( false );

	gbSizerGame->Add( spinCtrlFinishYear, wxGBPosition( 4, 2 ), wxGBSpan( 1, 1 ), 0, 5 );

	staticTextStartYear = new wxStaticText( panelGame, wxID_ANY, wxT("Start Year"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextStartYear->Wrap( -1 );
	gbSizerGame->Add( staticTextStartYear, wxGBPosition( 5, 0 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxALL, 5 );

	checkBoxStartYear = new wxCheckBox( panelGame, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	checkBoxStartYear->SetToolTip( wxT("Has start year") );

	gbSizerGame->Add( checkBoxStartYear, wxGBPosition( 5, 1 ), wxGBSpan( 1, 1 ), wxBOTTOM|wxRIGHT|wxTOP, 5 );

	spinCtrlStartYear = new wxSpinCtrl( panelGame, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( -1,-1 ), wxALIGN_CENTER_HORIZONTAL|wxSP_ARROW_KEYS, 1900, 9999, 1900 );
	spinCtrlStartYear->SetFont( wxFont( 10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxEmptyString ) );
	spinCtrlStartYear->Enable( false );

	gbSizerGame->Add( spinCtrlStartYear, wxGBPosition( 5, 2 ), wxGBSpan( 1, 1 ), 0, 5 );

	staticTextRegion = new wxStaticText( panelGame, wxID_ANY, wxT("Region"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextRegion->Wrap( -1 );
	gbSizerGame->Add( staticTextRegion, wxGBPosition( 6, 0 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxALL, 5 );

	checkBoxRegion = new wxCheckBox( panelGame, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	checkBoxRegion->SetToolTip( wxT("Has region") );

	gbSizerGame->Add( checkBoxRegion, wxGBPosition( 6, 1 ), wxGBSpan( 1, 1 ), wxBOTTOM|wxRIGHT|wxTOP, 5 );

	bcomboBoxRegion = new wxBitmapComboBox( panelGame, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 100,-1 ), 0, NULL, 0 );
	bcomboBoxRegion->SetFont( wxFont( 10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxEmptyString ) );
	bcomboBoxRegion->Enable( false );

	gbSizerGame->Add( bcomboBoxRegion, wxGBPosition( 6, 2 ), wxGBSpan( 1, 3 ), 0, 5 );

	staticTextPatch = new wxStaticText( panelGame, wxID_ANY, wxT("Patch"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextPatch->Wrap( -1 );
	gbSizerGame->Add( staticTextPatch, wxGBPosition( 7, 0 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxALL, 5 );

	checkBoxPatch = new wxCheckBox( panelGame, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	checkBoxPatch->SetToolTip( wxT("Is patched") );

	gbSizerGame->Add( checkBoxPatch, wxGBPosition( 7, 1 ), wxGBSpan( 1, 1 ), wxBOTTOM|wxRIGHT|wxTOP, 5 );

	bcomboBoxPatch = new wxBitmapComboBox( panelGame, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 150,-1 ), 0, NULL, 0 );
	bcomboBoxPatch->SetFont( wxFont( 10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxEmptyString ) );
	bcomboBoxPatch->Enable( false );

	gbSizerGame->Add( bcomboBoxPatch, wxGBPosition( 7, 2 ), wxGBSpan( 1, 3 ), 0, 5 );

	bpButtonOK = new wxBitmapButton( panelGame, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( 32,32 ), wxBU_AUTODRAW|wxBORDER_NONE );

	bpButtonOK->SetBitmap( wxBitmap( wxT("res/buttons/ok.gif"), wxBITMAP_TYPE_ANY ) );
	bpButtonOK->SetBitmapPressed( wxBitmap( wxT("res/buttons/ok-pressed.gif"), wxBITMAP_TYPE_ANY ) );
	bpButtonOK->SetBitmapFocus( wxBitmap( wxT("res/buttons/ok-focus.gif"), wxBITMAP_TYPE_ANY ) );
	bpButtonOK->SetToolTip( wxT("Save changes") );

	gbSizerGame->Add( bpButtonOK, wxGBPosition( 5, 6 ), wxGBSpan( 3, 1 ), wxALL, 0 );

	bpButtonCancel = new wxBitmapButton( panelGame, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( 32,32 ), wxBU_AUTODRAW|wxBORDER_NONE );

	bpButtonCancel->SetBitmap( wxBitmap( wxT("res/buttons/cancel.gif"), wxBITMAP_TYPE_ANY ) );
	bpButtonCancel->SetBitmapPressed( wxBitmap( wxT("res/buttons/cancel-pressed.gif"), wxBITMAP_TYPE_ANY ) );
	bpButtonCancel->SetBitmapFocus( wxBitmap( wxT("res/buttons/cancel-focus.gif"), wxBITMAP_TYPE_ANY ) );
	bpButtonCancel->SetToolTip( wxT("Close window") );

	gbSizerGame->Add( bpButtonCancel, wxGBPosition( 3, 6 ), wxGBSpan( 2, 1 ), wxALL, 0 );

	bpButtonDelete = new wxBitmapButton( panelGame, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( 32,32 ), wxBU_AUTODRAW|wxBORDER_NONE );

	bpButtonDelete->SetBitmap( wxBitmap( wxT("res/buttons/delete.gif"), wxBITMAP_TYPE_ANY ) );
	bpButtonDelete->SetBitmapDisabled( wxBitmap( wxT("res/buttons/delete-disabled.gif"), wxBITMAP_TYPE_ANY ) );
	bpButtonDelete->SetBitmapPressed( wxBitmap( wxT("res/buttons/delete-pressed.gif"), wxBITMAP_TYPE_ANY ) );
	bpButtonDelete->SetBitmapFocus( wxBitmap( wxT("res/buttons/delete-focus.gif"), wxBITMAP_TYPE_ANY ) );
	bpButtonDelete->Enable( false );
	bpButtonDelete->SetToolTip( wxT("Remove game") );

	gbSizerGame->Add( bpButtonDelete, wxGBPosition( 1, 6 ), wxGBSpan( 2, 1 ), wxALL, 0 );


	panelGame->SetSizer( gbSizerGame );
	panelGame->Layout();
	gbSizerGame->Fit( panelGame );
	bSizerGame->Add( panelGame, 1, wxEXPAND | wxALL, 5 );


	this->SetSizer( bSizerGame );
	this->Layout();
	bSizerGame->Fit( this );

	this->Centre( wxBOTH );

	// Connect Events
	this->Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( FrameGameView::OnClose ) );
	textCtrlName->Connect( wxEVT_KEY_DOWN, wxKeyEventHandler( FrameGameView::OnKeyPressed ), NULL, this );
	bcomboBoxSystem->Connect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( FrameGameView::OnCombobox ), NULL, this );
	bcomboBoxSystem->Connect( wxEVT_KEY_DOWN, wxKeyEventHandler( FrameGameView::OnKeyPressed ), NULL, this );
	bcomboBoxSystem->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( FrameGameView::OnCombobox ), NULL, this );
	bcomboBoxGenre->Connect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( FrameGameView::OnCombobox ), NULL, this );
	bcomboBoxGenre->Connect( wxEVT_KEY_DOWN, wxKeyEventHandler( FrameGameView::OnKeyPressed ), NULL, this );
	bcomboBoxGenre->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( FrameGameView::OnCombobox ), NULL, this );
	bcomboBoxCompletionStatus->Connect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( FrameGameView::OnCombobox ), NULL, this );
	bcomboBoxCompletionStatus->Connect( wxEVT_KEY_DOWN, wxKeyEventHandler( FrameGameView::OnKeyPressed ), NULL, this );
	bcomboBoxCompletionStatus->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( FrameGameView::OnCombobox ), NULL, this );
	checkBoxFinishYear->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( FrameGameView::OnFinishYearChecked ), NULL, this );
	spinCtrlFinishYear->Connect( wxEVT_KEY_DOWN, wxKeyEventHandler( FrameGameView::OnKeyPressed ), NULL, this );
	spinCtrlFinishYear->Connect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( FrameGameView::OnSpinCtrl ), NULL, this );
	checkBoxStartYear->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( FrameGameView::OnStartYearChecked ), NULL, this );
	spinCtrlStartYear->Connect( wxEVT_KEY_DOWN, wxKeyEventHandler( FrameGameView::OnKeyPressed ), NULL, this );
	spinCtrlStartYear->Connect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( FrameGameView::OnSpinCtrl ), NULL, this );
	checkBoxRegion->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( FrameGameView::OnRegionChecked ), NULL, this );
	bcomboBoxRegion->Connect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( FrameGameView::OnCombobox ), NULL, this );
	bcomboBoxRegion->Connect( wxEVT_KEY_DOWN, wxKeyEventHandler( FrameGameView::OnKeyPressed ), NULL, this );
	bcomboBoxRegion->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( FrameGameView::OnCombobox ), NULL, this );
	checkBoxPatch->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( FrameGameView::OnPatchChecked ), NULL, this );
	bcomboBoxPatch->Connect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( FrameGameView::OnCombobox ), NULL, this );
	bcomboBoxPatch->Connect( wxEVT_KEY_DOWN, wxKeyEventHandler( FrameGameView::OnKeyPressed ), NULL, this );
	bcomboBoxPatch->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( FrameGameView::OnCombobox ), NULL, this );
	bpButtonOK->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameGameView::OnOKClick ), NULL, this );
	bpButtonCancel->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameGameView::OnCancelClick ), NULL, this );
	bpButtonDelete->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameGameView::OnDeleteClick ), NULL, this );
}

FrameGameView::~FrameGameView()
{
	// Disconnect Events
	this->Disconnect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( FrameGameView::OnClose ) );
	textCtrlName->Disconnect( wxEVT_KEY_DOWN, wxKeyEventHandler( FrameGameView::OnKeyPressed ), NULL, this );
	bcomboBoxSystem->Disconnect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( FrameGameView::OnCombobox ), NULL, this );
	bcomboBoxSystem->Disconnect( wxEVT_KEY_DOWN, wxKeyEventHandler( FrameGameView::OnKeyPressed ), NULL, this );
	bcomboBoxSystem->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( FrameGameView::OnCombobox ), NULL, this );
	bcomboBoxGenre->Disconnect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( FrameGameView::OnCombobox ), NULL, this );
	bcomboBoxGenre->Disconnect( wxEVT_KEY_DOWN, wxKeyEventHandler( FrameGameView::OnKeyPressed ), NULL, this );
	bcomboBoxGenre->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( FrameGameView::OnCombobox ), NULL, this );
	bcomboBoxCompletionStatus->Disconnect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( FrameGameView::OnCombobox ), NULL, this );
	bcomboBoxCompletionStatus->Disconnect( wxEVT_KEY_DOWN, wxKeyEventHandler( FrameGameView::OnKeyPressed ), NULL, this );
	bcomboBoxCompletionStatus->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( FrameGameView::OnCombobox ), NULL, this );
	checkBoxFinishYear->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( FrameGameView::OnFinishYearChecked ), NULL, this );
	spinCtrlFinishYear->Disconnect( wxEVT_KEY_DOWN, wxKeyEventHandler( FrameGameView::OnKeyPressed ), NULL, this );
	spinCtrlFinishYear->Disconnect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( FrameGameView::OnSpinCtrl ), NULL, this );
	checkBoxStartYear->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( FrameGameView::OnStartYearChecked ), NULL, this );
	spinCtrlStartYear->Disconnect( wxEVT_KEY_DOWN, wxKeyEventHandler( FrameGameView::OnKeyPressed ), NULL, this );
	spinCtrlStartYear->Disconnect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( FrameGameView::OnSpinCtrl ), NULL, this );
	checkBoxRegion->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( FrameGameView::OnRegionChecked ), NULL, this );
	bcomboBoxRegion->Disconnect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( FrameGameView::OnCombobox ), NULL, this );
	bcomboBoxRegion->Disconnect( wxEVT_KEY_DOWN, wxKeyEventHandler( FrameGameView::OnKeyPressed ), NULL, this );
	bcomboBoxRegion->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( FrameGameView::OnCombobox ), NULL, this );
	checkBoxPatch->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( FrameGameView::OnPatchChecked ), NULL, this );
	bcomboBoxPatch->Disconnect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( FrameGameView::OnCombobox ), NULL, this );
	bcomboBoxPatch->Disconnect( wxEVT_KEY_DOWN, wxKeyEventHandler( FrameGameView::OnKeyPressed ), NULL, this );
	bcomboBoxPatch->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( FrameGameView::OnCombobox ), NULL, this );
	bpButtonOK->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameGameView::OnOKClick ), NULL, this );
	bpButtonCancel->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameGameView::OnCancelClick ), NULL, this );
	bpButtonDelete->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameGameView::OnDeleteClick ), NULL, this );

}
