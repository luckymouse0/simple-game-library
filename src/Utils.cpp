#include "Utils.h"

#include <wx/filename.h>
#include <wx/stdpaths.h>
#include <wx/dynarray.h>
#include <wx/image.h>
#include <wx/msgdlg.h>
#include <algorithm>
#include <codecvt>

namespace Utils
{
    wxString GetConfigKeyPrefix(wxWindow* frame)
    {
        return frame->GetName().MakeUpper() + "_WINDOW_";
    }

    void LoadFrameConfig(Config* config, wxWindow* frame)
    {
        wxString keysPrefix = GetConfigKeyPrefix(frame);

        wxString value;
        frame->SetSize((value = config->ReadKey(keysPrefix + "W")) != "" ? Utils::GetIntFromString(value) : frame->GetSize().GetWidth(), (value = config->ReadKey(keysPrefix + "H")) != "" ? Utils::GetIntFromString(value) : frame->GetSize().GetHeight());

        wxPoint position;
        position.x = (value = config->ReadKey(keysPrefix + "X")) != "" ? Utils::GetIntFromString(value) : frame->GetPosition().x;
        position.y = (value = config->ReadKey(keysPrefix + "Y")) != "" ? Utils::GetIntFromString(value) : frame->GetPosition().y;
        frame->SetPosition(position);
    }

    void SaveFrameConfig(Config* config, wxWindow* frame)
    {
        wxString keysPrefix = GetConfigKeyPrefix(frame);

        config->WriteKey(keysPrefix + "W", wxString(std::to_string(frame->GetSize().GetWidth())));
        config->WriteKey(keysPrefix + "H", wxString(std::to_string(frame->GetSize().GetHeight())));
        config->WriteKey(keysPrefix + "X", wxString(std::to_string(frame->GetPosition().x)));
        config->WriteKey(keysPrefix + "Y", wxString(std::to_string(frame->GetPosition().y)));
    }

    wxString GetExePath()
    {
        return wxFileName(wxStandardPaths::Get().GetExecutablePath()).GetPath();
    }

    wxSize GetMinimumSize(wxWindow* frame)
    {
        wxSize bestSize = frame->GetBestSize();

        // Take into consideration any frame decorations
        int frameDecorationsWidth = frame->GetSize().GetWidth() - frame->GetClientSize().GetWidth();
        int frameDecorationsHeight = frame->GetSize().GetHeight() - frame->GetClientSize().GetHeight();

        // Calculate the minimum size required to show all the elements
        wxSize minSize(bestSize.GetWidth() + frameDecorationsWidth, bestSize.GetHeight() + frameDecorationsHeight);

        return minSize;
    }

    int GetIntFromString(const wxString& value)
    {
        long l = 0;
        value.ToLong(&l);

        return static_cast<int>(l);
    }

    wxString RemoveSubstring(const wxString& input, const wxString& substring)
    {
        wxString output = input;

        size_t endIndex = input.find(substring);
        if (endIndex != wxString::npos)
        {
            output = input.substr(endIndex + substring.length());
        }

        return output;
    }

    wxArrayString GetColLabels(wxGrid* grid)
    {
        wxArrayString colLabels;

        for (int c = 0; c < grid->GetNumberCols(); c++)
        {
            colLabels.push_back(grid->GetColLabelValue(c));
        }

        return colLabels;
    }

    int GetColIndex(wxGrid* grid, const wxString& column)
    {
        int index = -1;

        for (int col = 0; col < grid->GetNumberCols(); col++)
        {
            if (grid->GetColLabelValue(col) == column)
            {
                index = col;
                break;
            }
        }

        return index;
    }

    wxArrayString GetSelectedRowValues(wxGrid* grid, int col)
    {
        wxArrayString rowValues;
        wxArrayInt rows = grid->GetSelectedRows();

        for (size_t i = 0; i < rows.Count(); i++)
        {
            rowValues.push_back(grid->GetCellValue(rows[i], col));
        }

        return rowValues;
    }

    wxArrayString GetColumnValues(wxGrid* grid, int col)
    {
        wxArrayString columnValues;

        for (int i = 0; i < grid->GetNumberRows(); i++)
        {
            columnValues.Add(grid->GetCellValue(i, col));
        }

        return columnValues;
    }

    bool MenuItemStartWith(wxMenu* menu, const wxString& label)
    {
        wxMenuItem* item = nullptr;
        for (size_t i = 0; i < menu->GetMenuItemCount(); i++)
        {
            wxMenuItem* currentItem = menu->FindItemByPosition(i);
            if (currentItem && currentItem->GetItemLabel().StartsWith(label))
            {
                item = currentItem;
                break;
            }
        }

        return item ? true : false;
    }

    void EnableRenameMenuItem(wxMenu* menu, const wxString& label, bool enabled, const wxString& newLabel)
    {
        wxMenuItem* item = nullptr;
        for (size_t i = 0; i < menu->GetMenuItemCount(); i++)
        {
            wxMenuItem* currentItem = menu->FindItemByPosition(i);
            if (currentItem && currentItem->GetItemLabel().StartsWith(label))
            {
                item = currentItem;
                break;
            }
        }

        if (item)
        {
            item->Enable(enabled);

            if (!newLabel.IsEmpty())
            {
                item->SetItemLabel(newLabel);
            }
        }
    }

    wxBitmap ResizeBitmap(const wxBitmap& bitmap, int width, int height)
    {
        wxImage image = bitmap.ConvertToImage();
        wxImage resizedImage = image.Rescale(width, height);
        wxBitmap resizedBitmap(resizedImage);

        return resizedBitmap;
    }

    bool IsNewComboElement(wxComboBox* comboBox)
    {
        bool isNew = true;
        wxString element = comboBox->GetValue();

        for (unsigned int i = 0; i < comboBox->GetCount(); i++)
        {
            if (element == comboBox->GetString(i))
            {
                isNew = false;
                break;
            }
        }

        return isNew;
    }

    bool IsNewComboElement(wxBitmapComboBox* bcomboBox)
    {
        return IsNewComboElement(dynamic_cast<wxComboBox*>(bcomboBox));
    }

    bool ConfirmAddNewComboElement(wxWindow* parent, wxComboBox* comboBox, const wxString& elementType, wxArrayString* arrayString)
    {
        bool added = true;

        wxMessageDialog dialog(parent, "Do you want to add the new " + elementType + ": " + comboBox->GetValue() + "?", "Confirm: Add new " + elementType, wxYES_NO | wxICON_QUESTION);
        if (dialog.ShowModal() == wxID_YES)
        {
            arrayString->push_back(comboBox->GetValue());
        }
        else
        {
            added = false;
        }

        return added;
    }

    bool ConfirmAddNewComboElement(wxWindow* parent, wxBitmapComboBox* bcomboBox, const wxString& elementType, wxArrayString* arrayString)
    {
        return ConfirmAddNewComboElement(parent, dynamic_cast<wxComboBox*>(bcomboBox), elementType, arrayString);
    }

    void AddComboBoxIcons(wxBitmapComboBox* bcomboBox, wxString iconsPath, int width, int height, const wxString& notFoundFileName)
    {
        for (unsigned int i = 0; i < bcomboBox->GetCount(); i++)
        {
            wxBitmap icon;
            wxString element = bcomboBox->GetString(i);
            element.Replace("/", "'");

            wxString filePath = iconsPath + element + ".gif";
            if (wxFile::Exists(filePath))
            {
                icon = Utils::ResizeBitmap(wxBitmap(filePath, wxBITMAP_TYPE_GIF), width, height);
            }
            else if (!notFoundFileName.IsEmpty())
            {
                icon = Utils::ResizeBitmap(wxBitmap(iconsPath + notFoundFileName, wxBITMAP_TYPE_GIF), width, height);
            }

            if (icon.IsOk())
            {
                bcomboBox->SetItemBitmap(i, icon);
            }
        }
    }

    bool IsNewElement(wxArrayString* arrayString, const wxString& element)
    {
        return arrayString->Index(element) == wxNOT_FOUND ? true : false;
    }

    void AddNewElement(wxArrayString* arrayString, const wxString& element)
    {
        if (!element.IsEmpty() && Utils::IsNewElement(arrayString, element))
        {
            arrayString->push_back(element);
        }
    }

    int CountElement(wxArrayString* arrayString, const wxString& element)
    {
        int count = 0;

        for (size_t e = 0; e < arrayString->Count(); e++)
        {
            if (arrayString->Item(e) == element)
            {
                count++;
            }
        }

        return count;
    }

    wxArrayString GetCategoriesCount(const wxArrayString& categories, const wxArrayString& items)
    {
        wxArrayString categoriesCount;

        for (const wxString& category : categories)
        {
            size_t count = 0;

            for (const wxString& item : items)
            {
                if (item == category)
                {
                    count++;
                }
            }

            wxString categoryCountString;
            categoryCountString << "[" << count << "] " << category;
            categoriesCount.Add(categoryCountString);
        }

        std::sort(categoriesCount.begin(), categoriesCount.end(), [&](const wxString& a, const wxString& b)
        {
            size_t countA = std::stoi(a.AfterFirst('[').BeforeFirst(']').ToStdString());
            size_t countB = std::stoi(b.AfterFirst('[').BeforeFirst(']').ToStdString());

            if (countA > countB)
            {
                return true;
            }
            else if (countA == countB)
            {
                // If the counts are equal, compare by the original order
                return categories.Index(a.AfterFirst(']').AfterFirst(' ').Trim()) < categories.Index(b.AfterFirst(']').AfterFirst(' ').Trim());
            }
            else
            {
                return false;
            }
        });

        return categoriesCount;
    }

    void RemoveUnusedElement(wxWindow* parent, wxArrayString* arrayString, wxComboBox* comboBox, const wxString& element)
    {
        if (element != comboBox->GetValue() && Utils::CountElement(arrayString, element) == 1)
        {
            wxMessageDialog dialog(parent, "Do you want to remove unused element: " + element + "?", "Confirm: Remove unused element", wxYES_NO | wxICON_QUESTION);
            if (dialog.ShowModal() == wxID_YES)
            {
                arrayString->Remove(element);
            }
        }
    }

    std::locale GetUTF8Locale()
    {
        return std::locale(std::locale(), new std::codecvt_utf8<wchar_t>);
    }

    std::string ConvertToUTF8(const wxString& s)
    {
        std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
        std::wstring wideStringValue = wxString::FromUTF8(s.ToUTF8()).wc_str();
        std::string utf8Value = converter.to_bytes(wideStringValue);

        return utf8Value;
    }
}
