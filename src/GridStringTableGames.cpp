#include "GridStringTableGames.h"

GridStringTableGames::GridStringTableGames(int cols) : GridStringTableGames(std::vector<Game>(), cols)
{
}

GridStringTableGames::GridStringTableGames(const std::vector<Game>& g, int cols) : games(g), columns(cols)
{
}

GridStringTableGames::~GridStringTableGames()
{
}

int GridStringTableGames::GetNumberRows()
{
    return games.size();
}

int GridStringTableGames::GetNumberCols()
{
    return columns;
}

wxString GridStringTableGames::GetValue(int row, int col)
{
    switch (col)
    {
        case 0: return games[row].GetName();
        case 1: return games[row].GetSystem();
        case 2: return games[row].GetGenre();
        case 3: return GetCompletionStatusString(games[row].GetCompletetionStatus());
        case 4: return wxString(std::to_string(games[row].GetFinishYear()));
        case 5: return wxString(std::to_string(games[row].GetStartYear()));
        default: return "";
    }
}

void GridStringTableGames::SetValue(int row, int col, const wxString& value)
{
    switch (col)
    {
        case 0: games[row].SetName(value); break;
        case 1: games[row].SetSystem(value); break;
        case 2: games[row].SetGenre(value); break;
        case 3: games[row].SetCompletetionStatus(GetCompletionStatusFromString(value)); break;
        case 4: games[row].SetFinishYear(GetIntFromString(value)); break;
        case 5: games[row].SetStartYear(GetIntFromString(value)); break;
    }
}

wxString GridStringTableGames::GetColLabelValue(int col)
{
    switch (col)
    {
        case 0: return "Name";
        case 1: return "System";
        case 2: return "Genre";
        case 3: return "Completion Status";
        case 4: return "Finish Year";
        case 5: return "Start Year";
        default: return "";
    }
}

void GridStringTableGames::SetGames(std::vector<Game>& g)
{
    games = g;
}

std::vector<Game> GridStringTableGames::GetGames()
{
    return games;
}

Game GridStringTableGames::GetGame(int position)
{
    return games[position];
}

void GridStringTableGames::SetGame(int position, Game game)
{
    games.insert(games.begin() + position, game);
}

wxString GridStringTableGames::GetCompletionStatusString(CompletionStatus status)
{
    switch (status)
    {
        case CompletionStatus::Unplayed: return "Unplayed";
        case CompletionStatus::Playing: return "Playing";
        case CompletionStatus::Completed: return "Completed";
        case CompletionStatus::Played: return "Played";
        case CompletionStatus::OnHold: return "OnHold";
        case CompletionStatus::Dropped: return "Dropped";
        default: return "";
    }
}

CompletionStatus GridStringTableGames::GetCompletionStatusFromString(wxString status)
{
    if (status == "Unplayed")
    {
        return CompletionStatus::Unplayed;
    }
    else if (status == "Playing")
    {
        return CompletionStatus::Playing;
    }
    else if (status == "Completed")
    {
        return CompletionStatus::Completed;
    }
    else if (status == "Played")
    {
        return CompletionStatus::Played;
    }
    else if (status == "OnHold")
    {
        return CompletionStatus::OnHold;
    }
    else if (status == "Dropped")
    {
        return CompletionStatus::Dropped;
    }
    else
    {
        return CompletionStatus::Unplayed;
    }
}

int GridStringTableGames::GetIntFromString(wxString value)
{
    long l = 0;
    value.ToLong(&l);

    return static_cast<int>(l);
}
