[![SGL logo](docs/images/logo.png)](https://gitlab.com/luckymouse0/simple-game-library)

# Simple Game Library

Simple Game Library is an open-source tool to organize and manage a gaming collection, adding games to a list and marking them as completed, currently playing, played, not played, on hold or dropped.

## Features

- Game List Management: Storing and managing games.
- Flexible Game Status Tracking: Categorize games based on their current status, be it completed, currently playing, played, unplayed, dropped, or on-hold.
- Detailed Game Information: Add game information including its title, genre, system, play status, completed / start year, release region, patches and so forth.
- Searching and Filtering: Find specific games within the library using filters like title, genre, or any other criteria.
- Sorting: Sort games by genre, release date, title, play status and so on.
- Data Import and Export: Import or export the game library data indicating what data to export.
- Customization: Add new or modify existing play status, genres, systems, etc.

## Screenshots

Linux - openSUSE - XFCE - Adwaita-dark theme

![](docs/images/sgl-opensuse-1-mainwin.png)

Windows 10

![](docs/images/sgl-win-1-mainwin.png)

## Tools used

- Code::Blocks 20.03
- wxWidgets 3.1.4
- wxFormBuilder 3.10.1-0-g8feb16b3

## Compiling from source

### Paths

- Linux: It uses the default compiled-from-source wxWidgets path.
- Windows: Add a global variable in Code::Blocks named "wx" with the following:
  - base: wxWidgets installation path.
  - include: wxWidgets "include" sub-folder path.
  - lib: wxWidgets "lib" sub-folder path.

### Build Options

- Linux: Use Debug or Release build options.
- Windows: Use DebugWin or ReleaseWin build options.

## License

Game Library is distributed under the LGPL 2.1 license, refer to the LICENSE file available in the project repository.
