#ifndef FRAMEMAIN_H
#define FRAMEMAIN_H

#include "Views.h"
#include "Config.h"
#include "Games.h"
#include "GridSorter.h"

#include <wx/window.h>
#include <wx/event.h>
#include <wx/grid.h>
#include <wx/string.h>
#include <wx/arrstr.h>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class FrameMain
///////////////////////////////////////////////////////////////////////////////
class FrameMain : public FrameMainView
{
    public:
        FrameMain(wxWindow* parent);
        ~FrameMain();
        Config* GetConfig();
        void UpdateGrid(Games* games);

    private:
        void OnClose(wxCloseEvent& event) override;
        void OnQuitWithoutSave(wxCommandEvent& event) override;
        void LoadGridConfig();
        void SaveGridConfig();
        void AddGamesToGrid(Games* games);
        void OnGridColSort(wxGridEvent& event) override;
        void GridSort(int col);
        void SortByColumn(int col);
        void OnHamburgerClick(wxMouseEvent& event) override;
        void OnAddNewGame(wxCommandEvent& event) override;
        void OnGamesCellLeftDClick(wxGridEvent& event) override;
        void OnGamesCellRightClick(wxGridEvent& event) override;
        void OnSelectAllRows(wxCommandEvent& event) override;
        void OnPinRows(wxCommandEvent& event) override;
        void OnUnpinAll(wxCommandEvent& event) override;
        void OnDeleteRows(wxCommandEvent& event) override;
        void OnGamesKeyDown(wxKeyEvent& event) override;
        void DeleteRows();
        void OnSearchFilter(wxCommandEvent& event) override;
        void OnChoiceFilter(wxCommandEvent& event) override;
        void OnListBoxFilter(wxCommandEvent& event) override;
        void FilterGrid(const wxString& column, const wxString& columnFilter, const wxString& searchFilter);
        void ClearFilters();
        void UpdateLists(Games* games);
        void UpdatePinned(Games* games);
        void SetPinnedRows();
        void SetPinnedRowsColor();

        wxString exePath;
        Config* config;
        Games* games;
        GridSorter sorter;
        wxString gamesPath;
        wxString pinnedPath;
        wxArrayString genres;
        wxArrayString systems;
        wxArrayString completionStatus;
        wxArrayString finishYears;
        wxArrayString startYears;
        wxArrayString regions;
        wxArrayString patches;
        int sortingColumn;
        wxArrayString pinnedGames;
};

#endif // FRAMEMAIN_H
