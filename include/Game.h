#ifndef GAME_H
#define GAME_H

#include <wx/string.h>

class Game
{
    public:
        Game(wxString name, wxString system, wxString genre, wxString completionStatus, unsigned int finishYear, unsigned int startYear, wxString region, wxString patch);
        virtual ~Game();
        wxString GetName();
        void SetName(wxString name);
        wxString GetSystem();
        void SetSystem(wxString system);
        wxString GetGenre();
        void SetGenre(wxString genre);
        wxString GetCompletionStatus();
        void SetCompletionStatus(wxString completionStatus);
        unsigned int GetFinishYear();
        void SetFinishYear(unsigned int finishYear);
        unsigned int GetStartYear();
        void SetStartYear(unsigned int startYear);
        wxString GetYear(bool isFinishYear);
        wxString GetRegion();
        void SetRegion(wxString region);
        wxString GetPatch();
        void SetPatch(wxString patch);

    private:
        wxString name;
        wxString system;
        wxString genre;
        wxString completionStatus;
        unsigned int finishYear;
        unsigned int startYear;
        wxString region;
        wxString patch;
};

#endif // GAME_H
