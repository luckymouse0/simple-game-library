#ifndef GRIDSORTER_H
#define GRIDSORTER_H

#include <string>
#include <vector>
#include <map>

class GridSorter
{
    public:
        GridSorter();
        virtual ~GridSorter();
        void ToggleSortDirection(int col);
        void SetSortDirection(int col, bool direction);
        bool GetSortDirection(int col);
        void SortData(int col, std::vector<std::vector<std::string>>& rowData, const std::vector<std::string>& pinnedRows);

    private:
        std::map<int, bool> sortDirection;
};

#endif // GRIDSORTER_H
