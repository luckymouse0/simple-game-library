#ifndef UTILS_H
#define UTILS_H

#include "Config.h"

#include <wx/string.h>
#include <wx/gdicmn.h>
#include <wx/window.h>
#include <wx/grid.h>
#include <wx/menu.h>
#include <wx/bitmap.h>
#include <wx/arrstr.h>
#include <wx/combobox.h>
#include <wx/bmpcbox.h>
#include <locale>
#include <string>

namespace Utils
{
    wxString GetConfigKeyPrefix(wxWindow* frame);
    void LoadFrameConfig(Config* config, wxWindow* frame);
    void SaveFrameConfig(Config* config, wxWindow* frame);
    wxString GetExePath();
    wxSize GetMinimumSize(wxWindow* parent);
    int GetIntFromString(const wxString& value);
    wxString RemoveSubstring(const wxString& input, const wxString& substring);
    wxArrayString GetColLabels(wxGrid* grid);
    int GetColIndex(wxGrid* grid, const wxString& column);
    wxArrayString GetSelectedRowValues(wxGrid* grid, int col);
    wxArrayString GetColumnValues(wxGrid* grid, int col);
    bool MenuItemStartWith(wxMenu* menu, const wxString& label);
    void EnableRenameMenuItem(wxMenu* menu, const wxString& label, bool enabled, const wxString& newLabel="");
    wxBitmap ResizeBitmap(const wxBitmap& bitmap, int width, int height);
    bool IsNewComboElement(wxComboBox* comboBox);
    bool IsNewComboElement(wxBitmapComboBox* comboBox);
    bool ConfirmAddNewComboElement(wxWindow* parent, wxComboBox* comboBox, const wxString& elementType, wxArrayString* arrayString);
    bool ConfirmAddNewComboElement(wxWindow* parent, wxBitmapComboBox* bcomboBox, const wxString& elementType, wxArrayString* arrayString);
    void AddComboBoxIcons(wxBitmapComboBox* bcomboBox, wxString iconsPath, int width, int height, const wxString& notFoundFileName="");
    bool IsNewElement(wxArrayString* arrayString, const wxString& element);
    void AddNewElement(wxArrayString* arrayString, const wxString& element);
    int CountElement(wxArrayString* arrayString, const wxString& element);
    wxArrayString GetCategoriesCount(const wxArrayString& categories, const wxArrayString& items);
    void RemoveUnusedElement(wxWindow* parent, wxArrayString* arrayString, wxComboBox* comboBox, const wxString& element);
    std::locale GetUTF8Locale();
    std::string ConvertToUTF8(const wxString& s);
};

#endif // UTILS_H
