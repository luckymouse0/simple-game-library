#ifndef FRAMEGAME_H
#define FRAMEGAME_H

#include "FrameMain.h"
#include "Views.h"
#include "Games.h"

#include <wx/window.h>
#include <wx/string.h>
#include <wx/arrstr.h>
#include <wx/event.h>
#include <wx/spinctrl.h>
#include <wx/checkbox.h>

class FrameGame : public FrameGameView
{
    public:
        FrameGame(FrameMain* parent, const wxString& title, const wxArrayString& genres, const wxArrayString& systems, const wxArrayString& completionStatus, const wxArrayString& regions, const wxArrayString& patches, Games* games, const int& row=-1);
        virtual ~FrameGame();

    private:
        void OnClose(wxCloseEvent& event) override;
        void SetSpinCtrlValue(wxSpinCtrl* spinCtrl, wxCheckBox* checkBoxYear, int year);
        void OnFinishYearChecked(wxCommandEvent& event) override;
        void OnStartYearChecked(wxCommandEvent& event) override;
        void OnRegionChecked(wxCommandEvent& event) override;
        void OnPatchChecked(wxCommandEvent& event) override;
        void OnOKClick(wxCommandEvent& event) override;
        void OnCancelClick(wxCommandEvent& event) override;
        void OnDeleteClick(wxCommandEvent& event) override;
        void OnKeyPressed(wxKeyEvent& event) override;
        void OnCombobox(wxCommandEvent& event) override;
        void OnSpinCtrl(wxSpinEvent& event) override;
        bool IsEdited();
        void SetEdited(bool changed=true);
        void UpdateSpinCtrlValues();
        bool CheckValues();
        bool CheckComboElements();

        FrameMain* parent;
        Games* games;
        wxArrayString* genres;
        wxArrayString* systems;
        wxArrayString* completionStatus;
        wxArrayString* regions;
        wxArrayString* patches;
        int row;
        bool initialized;
};

#endif // FRAMEGAME_H
