///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.10.1)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/srchctrl.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/choice.h>
#include <wx/bmpbuttn.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/button.h>
#include <wx/listbox.h>
#include <wx/grid.h>
#include <wx/gbsizer.h>
#include <wx/menu.h>
#include <wx/panel.h>
#include <wx/sizer.h>
#include <wx/frame.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
#include <wx/bmpcbox.h>
#include <wx/checkbox.h>
#include <wx/spinctrl.h>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class FrameMainView
///////////////////////////////////////////////////////////////////////////////
class FrameMainView : public wxFrame
{
	private:

	protected:
		wxPanel* panelMain;
		wxSearchCtrl* searchCtrlFilter;
		wxChoice* choiceFilter;
		wxBitmapButton* bpButtonHamburger;
		wxListBox* listBoxFilter;
		wxGrid* gridGames;
		wxMenu* menuGridContext;
		wxMenu* menuHamburger;

		// Virtual event handlers, override them in your derived class
		virtual void OnClose( wxCloseEvent& event ) { event.Skip(); }
		virtual void OnSearchFilter( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnChoiceFilter( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnHamburgerClick( wxMouseEvent& event ) { event.Skip(); }
		virtual void OnListBoxFilter( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnGamesCellLeftDClick( wxGridEvent& event ) { event.Skip(); }
		virtual void OnGamesCellRightClick( wxGridEvent& event ) { event.Skip(); }
		virtual void OnGridColSort( wxGridEvent& event ) { event.Skip(); }
		virtual void OnGamesKeyDown( wxKeyEvent& event ) { event.Skip(); }
		virtual void OnSelectAllRows( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnPinRows( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnUnpinAll( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnDeleteRows( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnAddNewGame( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnQuitWithoutSave( wxCommandEvent& event ) { event.Skip(); }


	public:

		FrameMainView( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Simple Game Library"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 976,488 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );

		~FrameMainView();

		void panelMainOnContextMenu( wxMouseEvent &event )
		{
			panelMain->PopupMenu( menuGridContext, event.GetPosition() );
		}

};

///////////////////////////////////////////////////////////////////////////////
/// Class FrameGameView
///////////////////////////////////////////////////////////////////////////////
class FrameGameView : public wxFrame
{
	private:

	protected:
		wxPanel* panelGame;
		wxStaticText* staticTextName;
		wxTextCtrl* textCtrlName;
		wxStaticText* staticTextSystem;
		wxBitmapComboBox* bcomboBoxSystem;
		wxStaticText* staticTextGenre;
		wxBitmapComboBox* bcomboBoxGenre;
		wxStaticText* staticTextCompletionStatus;
		wxBitmapComboBox* bcomboBoxCompletionStatus;
		wxStaticText* staticTextFinishYear;
		wxCheckBox* checkBoxFinishYear;
		wxSpinCtrl* spinCtrlFinishYear;
		wxStaticText* staticTextStartYear;
		wxCheckBox* checkBoxStartYear;
		wxSpinCtrl* spinCtrlStartYear;
		wxStaticText* staticTextRegion;
		wxCheckBox* checkBoxRegion;
		wxBitmapComboBox* bcomboBoxRegion;
		wxStaticText* staticTextPatch;
		wxCheckBox* checkBoxPatch;
		wxBitmapComboBox* bcomboBoxPatch;
		wxBitmapButton* bpButtonOK;
		wxBitmapButton* bpButtonCancel;
		wxBitmapButton* bpButtonDelete;

		// Virtual event handlers, override them in your derived class
		virtual void OnClose( wxCloseEvent& event ) { event.Skip(); }
		virtual void OnKeyPressed( wxKeyEvent& event ) { event.Skip(); }
		virtual void OnCombobox( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnFinishYearChecked( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSpinCtrl( wxSpinEvent& event ) { event.Skip(); }
		virtual void OnStartYearChecked( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnRegionChecked( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnPatchChecked( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnOKClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCancelClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnDeleteClick( wxCommandEvent& event ) { event.Skip(); }


	public:

		FrameGameView( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );

		~FrameGameView();

};

