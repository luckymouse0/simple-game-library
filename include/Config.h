#ifndef CONFIG_H
#define CONFIG_H

#include <wx/string.h>
#include <wx/fileconf.h>

class Config
{
    public:
        Config(wxString configPath);
        virtual ~Config();
        void Save();
        wxString ReadKey(wxString key);
        void WriteKey(wxString key, wxString value);
        void DeleteKey(wxString key);

    private:
        wxFileConfig* config;
};

#endif // CONFIG_H
