#ifndef FILEHANDLER_H
#define FILEHANDLER_H

#include "Games.h"
#include <wx/string.h>
#include <wx/arrstr.h>

namespace FileHandler
{
    bool SaveGames(wxString filename, Games* games, const wxArrayString& colLabels, bool header=true);
    Games* LoadGames(wxString filename, char delimiter=',', bool header=true);
    bool SaveFile(const wxString& filename, const wxArrayString& data);
    wxArrayString LoadFile(const wxString& filename);
    wxArrayString SplitString(wxString str, char delimiter=',');
}

#endif // FILEHANDLER_H
