#ifndef GAMES_H
#define GAMES_H

#include "Game.h"

#include <vector>
#include <wx/string.h>
#include <wx/arrstr.h>

class Games
{
    public:
        Games();
        virtual ~Games();
        int GetNumberRows() const;
        wxString GetValue(int row, int col);
        void SetValue(int row, int col, const wxString& value);
        Game* GetGame(int row);
        void AddGame(Game* game);
        void ModifyGame(int row, Game* game);
        void DeleteGame(int row);
        void DeleteGames(wxArrayString names);
        int GetGameRow(const wxString& name, int ignoreRow=-1);

    private:
        std::vector<Game*> games;
};

#endif // GAMES_H
