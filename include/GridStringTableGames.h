#ifndef GRIDSTRINGTABLEGAMES_H
#define GRIDSTRINGTABLEGAMES_H

#include "Game.h"
#include "CompletionStatus.h"

#include <wx/grid.h>
#include <wx/arrstr.h>

class GridStringTableGames : public wxGridStringTable
{
    public:
        GridStringTableGames(int cols);
        GridStringTableGames(const std::vector<Game>& g, int cols);
        virtual ~GridStringTableGames();
        int GetNumberRows() override;
        int GetNumberCols() override;
        wxString GetValue(int row, int col) override;
        void SetValue(int row, int col, const wxString& value) override;
        wxString GetColLabelValue(int col) override;
        std::vector<Game> GetGames();
        void SetGames(std::vector<Game>& games);
        Game GetGame(int position);
        void SetGame(int position, Game game);

    private:
        wxString GetCompletionStatusString(CompletionStatus status);
        CompletionStatus GetCompletionStatusFromString(wxString status);
        int GetIntFromString(wxString value);

        std::vector<Game> games;
        int columns;
};

#endif // GRIDSTRINGTABLEGAMES_H
